const express = require('express')
const path = require('path')
const bodyParser = require('body-parser');
const formidable = require('formidable')
const _ = require('lodash')

const app = express()

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({extended: true}));

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,content-type,Authorization");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    next();
});

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/:type/upload', function (req, res, next) {
    console.log(req);
    const form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, './', 'public', req.params.type);
    form.keepExtensions = true;	 //保留后缀

    let fileList = [];

    form.parse(req, function (err, fields, files) {
        console.log(fields);
        _.each(files, function (file) {
            console.log(file.path)
            let arr = file.path.split('/');
            let prefix = `/${req.params.type}/`
            fileList.push(prefix + arr[arr.length - 1]);
        })
        res.json(fileList);
    });
})

function logErrors(err, req, res, next) {
    if (typeof err === 'string')
        err = new Error(err);
    console.log(err)
    console.error('logErrors', err.toString());
    if(err.name == 'UnauthorizedError'){
        res.status(500).json({error: err.message});
        return;
    }
    next(err);
}

function clientErrorHandler(err, req, res, next) {
    if (req.xhr) {
        console.error('clientErrors response' + err.toString());
        res.status(500).json({error: err.toString()});
    } else {
        next(err);
    }
}

function errorHandler(err, req, res, next) {
    console.error('lastErrors response');
    //  res.status(500).send(err.toString());
    res.status(500).json({error: err.toString()});
}


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log('404 error');
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// error handlers
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// 实际发布时，采用多种的异常处理机制，可以增加服务器的稳定性
//其实对于这种情况，还是没有办法实现最佳的解决方案的。
// 现在对于非预期的异常产生的时候，我们只能够让当前请求超时，然后让这个进程停止服务，之后重新启动。
// graceful模块配合cluster就可clear以实现这个解决方案。
process.on('uncaughtException', function (err) {
    //打印出错误
    console.log(" on uncaughtException");
    console.log(err);
    //打印出错误的调用栈方便调试
    console.log(err.stack);
});

module.exports = app;


